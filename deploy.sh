#!/bin/bash
set -eo pipefail
aws cloudformation package --template-file template.yml --s3-bucket kone-assignment-lambda-deployment-artifacts --output-template-file out.yml
aws cloudformation deploy --template-file out.yml --stack-name kone-api-stack --capabilities CAPABILITY_NAMED_IAM